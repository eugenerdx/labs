//
//  task3.c
//  lab.3.4.
//
//  Created by eugenerdx on 19.11.15.
//  Copyright © 2015 eugenerdx. All rights reserved.
//

#include <stdio.h>
#include <string.h>
#define N 4

int main ()
{
    int max_dig=0,str_len;
    char Arr[80],temp[2]={0};
    int i;
    int dig=0,count=1;
    
    printf ("Введите непрерывную последовательность чисел в строку:\n");
    fgets (Arr);
    Arr[strlen(Arr) - 1] = 0;
    printf ("%s\n", Arr);
    str_len=strlen(Arr);
    for (i=0;i<=str_len;i++)
    {
        if (isdigit(Arr[i]) && count<N)
        {
            temp[0]=Arr[i];
            dig=dig*10+atoi(temp);
            count++;
        }
        else
        {
            max_dig+=dig;
            dig=0;
            count=1;
        }
    }
    printf ("max=%d\n", max_dig); 
    return 0;
}