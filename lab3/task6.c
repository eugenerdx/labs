//
//  task6.c
//  laba3.6
//
//  Created by eugenerdx on 21.11.15.
//  Copyright © 2015 eugenerdx. All rights reserved.
//

#include <stdio.h>
#include <time.h>
#include <stdlib.h>
# define N 10


int main()
{
    int dig[N];
    int i=0, max=0, min=0, sum=0;
    srand((int)time(0));
    for (i = 0; i < N; i++)
    {
        switch (rand() % 2)
        {
            case 0:
                dig[i] = rand() % 11 + '0';
                dig[i] = dig[i] * (-1);
                break;
            case 1:
                dig[i] = rand() % 11 + '0';
                break;
        }
    }
    printf("Массив выводимый на печать\n");
    
    for (i=0;i<=N-1;i++)
        printf("%d\t%d\n",i, dig[i]);
    

    for (i = 0; i < N; i++)
        if (dig[i] > max)
        {
            max = dig[i];
        }
    
    
    for (i = 0; i < N; i++)
        
        if (dig[i] < min)
        {
            min = dig[i];
        }
    
    printf("Максимальный и минимальный элемент в массиве: %d\t%d\n",max, min);
    
    sum = min + max;
    printf("Сумма двух элементов: %d %d = %d\n\n",max, min, sum);
    return 0;
}