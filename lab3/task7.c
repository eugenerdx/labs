//
// task7.c
// 3.7
//
// Created by eugenerdx on 25.11.15.
// Copyright � 2015 eugenerdx. All rights reserved.
//

#include <stdio.h>
#include <string.h>
#define N 255

int main()
{
	int code[N];
	int freq[N];
	char Str[N];
	int i, j, k;

	for (i = 0; i<N; i++)
	{
		code[i] = i;
		freq[i] = 0;
	}
	for (i = 0; i<N; i++)
		Str[i] = 0;
	puts("������� ������, ���������� ��������� ���������� ������������� ��������:");
	fgets(Str, sizeof(Str), stdin);
	Str[strlen(Str) - 1] = 0;
	for (i = 0; i<strlen(Str); i++)
	{
		k = Str[i];
		freq[k]++;
	}
	for (i = 0; i<N; i++)
		for (j = i + 1; j<N; j++)
			if (freq[j] > freq[i])
			{
				k = freq[j];
				freq[j] = freq[i];
				freq[i] = k;
				k = code[j];
				code[j] = code[i];
				code[i] = k;
			}
	for (i = 0; i<N; i++)
		if (freq[i])
			printf("������ %c ����������� %d ���\n", code[i], freq[i]);

	return 0;
}
