//
// task5.c
// laba3.5
//
// Created by eugenerdx on 20.11.15.
// Copyright © 2015 eugenerdx. All rights reserved.
//

#include <stdio.h>
#include <time.h>
#include <stdlib.h>

# define N 10

int main()
{
	int dig[N];
	int i, summ = 0, lp = 0, fn = 0;
	int neg = 0;
	srand((unsigned int)time(0));
	for (i = 0; i < N; i++) {
		switch (rand() % 2)
		{
		case 0:
			dig[i] = rand() % 11 + '0';
			dig[i] = dig[i] * (-1);
			break;
		case 1:
			dig[i] = rand() % 11 + '0';
			break;
		}
	}
	puts("Случайный массив:");
	for (i = 0; i < N; ++i) {
		printf("%d: %d \n", i, dig[i]);
	}
	for (i = 0; i < N; i++) {
		if (dig[i] > 0) {
			lp = i;
		}
		else if (dig[i] < 0) {
			if (!neg) {
				fn = i;
				neg = 1;
			}
		}
	}
	if (fn > 0 && lp > 0) {
		for (i = fn + 1; i < lp; i++) {
			summ += dig[i];

		}
		printf("Первое отрицательное число в строке находится в ячейке № %d\n", fn);
		printf("Последнее положительное число в строке находится в ячейке №%d\n", lp);
		printf("Сумма всех чисел стоящих между ними равна %d\n", summ);

	}
}
