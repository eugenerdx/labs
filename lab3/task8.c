#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define M 256

int main()
{
    char str[M];
    int n=0;
    short inWord=0;
    int i=0, count=0;
    puts("Введите в строку несколько слов:");
    fgets(str,M,stdin);
    if(str[strlen(str)-1]=='\n')
        str[strlen(str)-1]=0;
    
    printf("Введите номер слова которе вы бы хотели увидеть на экране: \n");
    scanf("%d", &n);
    
    while(str[i])
    {
        if (str[i]!=' ' && inWord==0)
        {
            inWord=1;
            count++;
            if(n==count)
            {
            putchar(str[i]);
            }
        }
        else if(str[i]!=' ' && inWord==1 && n==count)
        {
            putchar(str[i]);
        }
        else if(str[i]==' ' && inWord==1)
        {
        
            inWord=0;
        }
        i++;
    }
    if(n>count)
    {
        printf("Слово №%d не может быть распечатано\n\n", n);
    }
    else if (n==count)
    {
    putchar(' ');
    printf("- это и есть %d слово в строке\n\n",n);
    }
    return 0;
}