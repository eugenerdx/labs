//
//  task2.c
//  lab3.2
//
//  Created by eugenerdx on 15.11.15.
//  Copyright © 2015 eugenerdx. All rights reserved.
//

#include <stdio.h>

int main()
{
    char str[]=" hello how are  you doing? ";
    int inWord=0, count=0, i=0;
    
    while(str[i])
    {
        if (str[i]!=' ' && inWord==0)
        {
            inWord=1;
            count++;
            putchar('\n');
            putchar(str[i]);
        }
        else if(str[i]!=' ' && inWord==1)
            putchar(str[i]);
        else if(str[i]==' ' && inWord==1)
            inWord=0;
        i++;
    }
    printf("\n\nКоличество строк = %d\n\n", count);
    return 0;
}

