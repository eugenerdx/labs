#include <stdio.h>
#include <stdlib.h>
#include <strings.h>

#define N 256
int main()

{
    char str[N];
    char tmp='0';
    int i=0,max=0,count = 1;
    puts("Введите строку:");
    fgets(str,sizeof(str),stdin);
    str[strlen(str)-1]=0;
    for(i=0; i<strlen(str); i++)
    {
        if(str[i]==str[i+1])
        {
            count++;
        }
        else
        {
            count++;
            if (count>max)
            {
                max=count;
                tmp=str[i];
                
            }
            count = 0;
        }
    }
    printf ("Последовательность - ");
    for (i=0; i<max; i++)
    {
        printf ("%c",tmp);
    }
    printf (" встречается %d раз\n",max);
    return 0;
}