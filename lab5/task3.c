
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#define N 256
#define M 256

char getWord(char *Arr, char **pArr, int *j);
char printWord(char *pArr);

int main ()
{
    char Arr[N][M];
    char *pArr[N];
    int i=0, j, k;
    FILE *fl;
    fl = fopen("53.txt","r");
    if (!fl)
    {
        printf("Ошибка чтения файла!");
        return 1;
    }
    while (fgets (Arr[i],M,fl))
    {
        if (Arr[i][0]=='\n')
            break;
        else
        {
            j=i;
            printf ("%s", Arr[i]);
            getWord(Arr[i], pArr, &i);
            for (k=j; k<i; k++)
                printWord(pArr[k]);
        }
    }
    fclose (fl);
    return 0;
}

char printWord(char *pArr)
{
    int i, j, count;
    char temp;
    srand((unsigned int)time(0));
    count=strlen(pArr)-1;
    for (i=1; i<count-1; i++)
    {
        j=rand()%(count-i-1);
        temp=pArr[i];
        pArr[i]=pArr[i+j];
        pArr[i+j]=temp;
    }
    printf ("%s \n", pArr);
    printf ("\n");
    return *pArr;
}

char getWord(char *Arr, char **pArr, int *j)
{
    int i, inWord=0, str=(strlen(Arr)-1);
    
    for(i=0; i<str; i++)
    {
        if((Arr[i]!=' ' || Arr[i] != '\n') && inWord==0)
        {
            inWord=1;
            pArr[*j]=Arr+i;
            (*j)++;
        }
        else if ((Arr[i] == ' ' || Arr[i] == '\n') && inWord==1)
        {
            inWord=0;
            Arr[i]='\0';
        }
    }
    return **pArr;
}
