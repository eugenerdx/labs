
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <stdlib.h>

char getWord(char *Arr, char **pArr, int *j);
void printWord(char *temp);

int main()
{
    char Arr[256];
    char *pArr[256];
    char *temp;
    int str,j=0, count;
    srand((unsigned int)time(0));
    printf ("Enter string\n");
    fgets(Arr,sizeof(Arr),stdin);
    Arr[strlen(Arr)-1]=0;
    getWord(Arr, pArr, &j);
    str=strlen(pArr);
    for(str=0; str<j; str++)
    {
        count=rand()%(j-str);
        printWord(pArr[count]);
        temp=pArr[count];
        pArr[count]=pArr[j-str-1];
        pArr[j-str-1]=temp;
    }
    printf ("\n");
    return 0;
}

char getWord(char *Arr, char **pArr, int *j)
{
    int i, inWord=0, str=strlen(Arr);
    for(i=0; i<str; i++)
    {
        if((Arr[i]!=' ' || Arr[i] != '\n') && inWord==0)
        {
            inWord=1;
            pArr[*j]=Arr+i;
            (*j)++;
        }
        else if ((Arr[i] == ' ' || Arr[i] == '\n') && inWord==1)
        {
            inWord=0;
            Arr[i]='\0';
        }
    }
    return **pArr;
}


void printWord(char *temp)
{
    while(*temp !='\0')
    {
        putchar(*temp);
        if(*(temp+1) != '\0')
            temp++;
        else
            break;
        
    }
    putchar (' ');
}
