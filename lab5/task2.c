//
//  main.c
//  lab5.2.
//
//  Created by eugenerdx on 05.12.15.
//  Copyright © 2015 eugenerdx. All rights reserved.
//


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

#define N 20
#define M 40

void clearArray(char(*Arr)[M])
{
    int i,j;
    for(i=0;i<N;i++)
        for(j=0;j<M;j++)
            Arr[i][j] = ' ';
}

void newArray(char(*Arr)[M])
{
    
    int i,j;
    for(i=0; i<N/2; i++)
        for(j=0;j<M/2;j++)
            Arr[i][M-j-1]=Arr[i][j];
    for(i=0; i<N/2; i++)
        for(j=0; j<M; j++)
            Arr[N-i-1][j]=Arr[i][j];
}

void clearScreen()
{
    system("clear");
}

void printArray(char(*Arr)[M])
{
    int i,j;
    for(i=0; i<N; i++)
    {
        for(j=0; j<M; j++)
            printf("%c", Arr[i][j]);
            printf("\n");
    }
}

void unixsleep(int delay)
{
    clock_t now = clock(); while(clock() < now + delay);
}

int kaleidoscop()
{
    int i,j;
    char Arr[N][M];
    while(1)
    {
        clearArray(Arr);
        srand((unsigned int)time(0));
        for(i=0;i<N/2; i++)
            for(j=0;j<M/2; j++)
            {
                if((rand()%2)==0)
                    Arr[i][j]='*';
                
            }
        clearScreen();
        newArray(Arr);
        printArray(Arr);
        unixsleep(10000000);
    }
    return 0;
}

int main()
{
    do
        kaleidoscop();
    while (!&kaleidoscop);
}