//
//  main.c
//  lab5.4
//
//  Created by eugenerdx on 05.12.15.
//  Copyright © 2015 eugenerdx. All rights reserved.
//
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#define N 256
#define M 256

void printWord(char *temp);
char getWord(char *Arr, char **pArr, int *j);

int main()
{
    char Arr[N][M];
    char *pArr[N];
    char temp;
    int i=0, j = 0, count, k;
    FILE *fl;
    fl = fopen("54.txt", "r");
    srand((unsigned int)time(0));
    if(!fl)
    {
        printf("Ошибка чтения файла");
        return 1;
    }
    while(fgets(Arr[i], M, fl))
    {
        if (Arr[i][j] == '\n')
            break;
        else
        {
            k=i;
            printf("%s", Arr[i]);
            getWord(Arr[i], pArr, &i);
            j=i;
            for(i=k; i<j; i++)
            {
                count=rand()%(j-i);
                printWord(pArr[k+count]);
                temp=*pArr[k+count];
                pArr[k+count]=pArr[k+j-i-1];
                pArr[k+j-i-1]=&temp;
            }
            printf("\n\n");
        }
    }
    fclose(fl);
    return 0;
}

char getWord(char *Arr, char **pArr, int *j)
{
    int i, inWord=0, str=strlen(Arr);
    for(i=0; i<str; i++)
    {
    if((Arr[i]!=' ' || Arr[i] != '\n') && inWord==0)
    {
        inWord=1;
        pArr[*j]=Arr+i;
        (*j)++;
    }
    else if ((Arr[i] == ' ' || Arr[i] == '\n') && inWord==1)
    {
        inWord=0;
        Arr[i]='\0';
    }
}
return **pArr;
}

void printWord(char *temp)
{
    while(*temp != '\0')
    {
        putchar(*temp);
        if(*(temp+1) != '\0')
            temp++;
        else break;
    }
    putchar(' ');
}