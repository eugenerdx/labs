//  main.c
//  lab.4.3
//
//  Created by eugenerdx on 29.11.15.
//  Copyright © 2015 eugenerdx. All rights reserved.
//
#include <stdio.h>
#include <string.h>

int main()
{
    char Arr[80];
    char *pArr[2];
    int i, j, count;
    int  str;
    printf ("Введите строку, чтобы проверить ее на 'палиндром'\n");
    fgets (Arr,sizeof(Arr),stdin);
    Arr[strlen(Arr)-1]=0;
    str=strlen(Arr);
    count=0;
    pArr[0]=Arr;
    pArr[1]=Arr+str-1;
    for (i=0, j=str-1; i<=j; i++, j--)
    {
        if (*(pArr[0]+i) !=* (pArr[1]-(str-1-j)))
        {
            printf ("В данной строке 'палиндром' не найден\n");
            count = 0;
            break;
        }
        else
            count++;
    }
    if (count == i)
        printf ("Введенная вами строка прошла проверку на 'палиндром'\n");
    return 0;
}