//
//  main.c
//  lab4.4
//
//  Created by eugenerdx on 01.12.15.
//  Copyright © 2015 eugenerdx. All rights reserved.
//

# include <stdio.h>
# include <string.h>

# define N 256
int main () {
    char Str[N];
    char *count, *beg, *end;
    int max = 0, i = 0, j = 1;
    int sym = '\0';
    printf("Введите строку с пробелом вконце, чтобы определить максимальную последовательность символов:\n");
    fgets (Str, N, stdin);
    Str[strlen (Str) - 1] = 0;
    beg = count = Str;
    end = count + strlen(Str);
    for (count = Str + 1; count<end; count++) {
        if (*count == *beg) {
            j++;
        }
        else {
            if (j > max) {
                max = j;
                sym = *beg;
            }
            j=1;
            *beg = *count;
        }
    }
    printf ("Символ %c, встречается %d раз:  ", sym, max);
    for (i = 0; i < max; i++)
    putchar (sym);
    putchar ('\n');
    return 0;
}
