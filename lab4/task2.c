//
//  main.c
//  lab.4.1..
//
//  Created by eugenerdx on 29.11.15.
//  Copyright © 2015 eugenerdx. All rights reserved.
//

#include <stdio.h>
#include <string.h>
#define N 80

int main ()
{
    char Arr[N];
    char *pArr[N];
    int i = 0, count=0;
    int inWord = 0;
    printf ("Введите слова в строку, чтобы вывести их наоборот\n");
    fgets (Arr,sizeof(Arr),stdin);
    Arr[strlen(Arr)-1]=0;
    while (Arr[i])
    {
        if (Arr[i] != ' ' && inWord == 0)
        {
            inWord = 1;
            pArr[count++] = &Arr[i];
        }
        else if (Arr[i] == ' ' && inWord == 1)
        {
            Arr[i] = '\0';
            inWord = 0;
        }
        i++;
    }
    for (i = count-1; i >= 0; i--)
    printf ("%s ", pArr[i]);
    printf ("\n");
    return 0;
}