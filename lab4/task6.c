//
//  main.c
//  4.6
//
//  Created by eugenerdx on 02.12.15.
//  Copyright © 2015 eugenerdx. All rights reserved.
//

#include <stdio.h>
#define LIM 256
#define SIZE 256

int main ()
{
    char Name[LIM][SIZE];
    int Age[LIM];
    int *young = 0, *old = 0;
    char *young_name = 0, *old_name = 0;
    int i, n;
    
    printf ("Введите количество родственников\n");
    scanf ("%d", &n);
    
    i=0;
    printf ("Введите имя, а затем возраст через пробел:\n");
    while (i<n)
    {
        fflush(stdin);
        scanf("%s %d", Name[i], Age+i);
        i++;
        
    }
    young=Age;
    old=Age;
    for (i=0; i<n; i++)
    {
        if(Age[i] <= *young)
        {
            young = &Age[i];
            young_name = Name[i];
        }
        if(Age[i] >= *old)
        {
            old = &Age[i];
            old_name = Name[i];
        }
    }
    printf ("\n\nМладшим является %s %d.\nСтаршим является %s %d.\n", young_name, *young, old_name, *old);
    return 0;
}
