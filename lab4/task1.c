//
//  main.c
//  lab.4.1.
//
//  Created by eugenerdx on 29.11.15.
//  Copyright © 2015 eugenerdx. All rights reserved.
//

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define SIZE 200
#define LIM 10
int main ()
{
    char Arr[LIM][SIZE];
    char *Adr[LIM];
    char *temp;
    int i=0, j, k;
    printf ("Введите строку\n");
    while(i<LIM && fgets(Arr[i],sizeof(Arr),stdin) != NULL && Arr[i][0] != '\0')
    {
        Adr[i]=&Arr[i][0];
        i++;
    }
    for (k=0; k<i; k++)
    {
        temp=Adr[k];
        for (j=0; j<i-1; j++)
        { 
            if (strlen(Adr[j]) > strlen(Adr[j+1]))
            {
                temp=Adr[j];
                Adr[j]=Adr[j+1];
                Adr[j+1]=temp;
            }
        }
    }
    for (k=0; k<i; k++)
        printf ("%s\n", Adr[k]);
    return 0;
}