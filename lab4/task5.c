//
//  main.c
//  4.5
//
//  Created by eugenerdx on 02.12.15.
//  Copyright © 2015 eugenerdx. All rights reserved.
//


#include <stdio.h>
#include <string.h>
#define SIZE 256
#define LIM 1000
int main()
{
    char Arr[LIM][SIZE];
    char *Adr[LIM];
    char *temp;
    int i=0, j, k;
    FILE *fp;
    FILE *sort;
    fp = fopen("file.txt", "r");
    sort = fopen("sort.txt", "w");
    if(!fp)
    {
        printf("Файл не может быть прочитан.\n");
        return 1;
    }
    while (i<LIM && fgets(Arr[i], LIM, fp) != NULL && Arr[i][0] != '\0')
    {
        if (Arr[i][0]=='\n')
            break;
        else
        {
            Adr[i]=&Arr[i][0];
            i++;
        }
    }
    
    for (k=0; k<i; k++)
    {
        temp=Adr[k];
        for (j=0; j<i-1; j++)
        {
            if (strlen(Adr[j]) > strlen(Adr[j+1]))
            {
                temp=Adr[j];
                Adr[j]=Adr[j+1];
                Adr[j+1]=temp;
            }
        }
    }
    fprintf (sort,"%s\n", "Сортированная строка:\n");
    for (k=0; k<i; k++)
    {
        fprintf (sort,"%s",Adr[k]);
        printf ("%s",Adr[k]);
    }
    fclose (fp);
    fclose (sort);
    return 0;
}