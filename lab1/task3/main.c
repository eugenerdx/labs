#include<stdio.h>
#include <math.h>
int main()
{
	float ugol;
	char c;
	const double pi = 3.14;
	printf("Enter angle: ");
	scanf("%f%c", &ugol, &c);
	if (c == 'D') printf("R=%f", pi / 180 * ugol);
	else printf("D=%f", 180 / pi*ugol);
	getch();
	return 0;
}