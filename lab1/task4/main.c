//
//  main.c
//  task4
//
//  Created by eugenerdx on 21.10.15.
//  Copyright © 2015 eugenerdx. All rights reserved.
//

#include <stdio.h>

int main()
{
	int f, d;
	printf("****Введите два числа 1 - футы, 2 - дюймы для перевода в европейскую систему****\n");
	scanf("%d%d", &f, &d);
	double sm = (f * 12 + d)*2.54;
	printf("%.1f сантиметров", sm);
	getch();
	return 0;
}
