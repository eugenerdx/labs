//
//  main.c
//  task2
//
//  Created by eugenerdx on 14.10.15.
//  Copyright © 2015 eugenerdx. All rights reserved.
//

#include <stdio.h>

int main()
{
    int hr, mn, sec;
    printf("Введите время: \n");
    while (1)
    {
        scanf("%d:%d:%d", &hr, &mn, &sec);
        hr = hr % 24;
        printf("%d\n", hr);
        if (hr<12 && hr >= 5)
            printf("Доброе утро:\n");
        else if (hr >= 12 && hr <= 23)
            printf("Добрый вечер:\n");
        else if (hr >= 24 && hr <= 4) 
            printf("Доброй ночи:\n"); 
        }
        return 0;
        
}